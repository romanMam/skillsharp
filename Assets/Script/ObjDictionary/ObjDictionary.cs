﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// SpaceObj Dictionary
/// </summary>
public class ObjDictionary : MonoBehaviour
{
    //Create Pool Elemets in Inspector
    [System.Serializable]
    public class ObjectPool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }

    //Access DictioneryInstance with Singleton
    #region DictioneryInstance Singleton
    public static ObjDictionary DictionaryInstance;

    private void Awake()
    {
        DictionaryInstance = this;
        Debug.Log("Dictionary Instance" + DictionaryInstance.name);
    }
    #endregion

    //Reffer to List of Pools
    public List<ObjectPool> objectPools;
    //Reffer to Dictionery Object
    public Dictionary<string, Queue<GameObject>> dictionaryPool;
    private Queue<GameObject> poolPref = new Queue<GameObject>();//Ready
    private List<GameObject> disPoolPref = new List<GameObject>();//Disabled

    //Instantiate Pools Objects on Start
    void Start()
    {
        dictionaryPool = new Dictionary<string, Queue<GameObject>>();

        foreach (ObjectPool pool in objectPools)
        {
            Queue<GameObject> prefab = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject prefabClone = Instantiate(pool.prefab);
                prefabClone.SetActive(false);
                prefab.Enqueue(prefabClone);
            }

            dictionaryPool.Add(pool.tag, prefab);
            Debug.Log("Start PoolInitialise: " + "Name: " + pool.tag + " sSize: " + pool.size);
        }
    }

    //ENQUEUE/DEQUEUE Object from Pool
    public GameObject ObjReuse(string poolTag, Vector3 position, Quaternion rotation)
    {
        if (!dictionaryPool.ContainsKey(poolTag))
        {
            Debug.LogWarning("Pool with tag " + poolTag + " doesn't exist");
            return null;
        }

        Debug.Log("SpawnObject Method!");
        //Remove from Queue, Set Position,Rotaton,Active
        GameObject spawnPrefab = dictionaryPool[poolTag].Dequeue();
        spawnPrefab.SetActive(true);
        spawnPrefab.transform.position = position;
        spawnPrefab.transform.rotation = rotation;

        IPoolObj pooledObject = spawnPrefab.GetComponent<IPoolObj>();
        Debug.Log("Spawn Object!");
        if (pooledObject != null)
        {
            pooledObject.ObjectPool();
        }

        dictionaryPool[poolTag].Enqueue(spawnPrefab);

        return spawnPrefab;//Return Prefab
    }
    
}
