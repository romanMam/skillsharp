﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// LaserShots Dictionary
/// </summary>
public class LaserPooler : MonoBehaviour
{
    //Inspector info about Laser Dictioner
    [System.Serializable]
    public class InspectPool
    {
        public string nameTag; //Pool Name
        public GameObject prefab; //Pool Prefab
        public int poolSize; //Pool Size, object amount
    }

    //Deffine LaserDictionery
    #region LaserDictioneryInstance
    //Refference access to Dictionery Object
    public static LaserPooler LaserInstance;

    //Deffine Dictionery Object on Scene
    void Awake()
    {
        LaserInstance = this;
    }
    #endregion

    //LaserPool Instanec
    public List<InspectPool> poolOfLasers;
    //Dictionery Refference
    public Dictionary<string, Queue<GameObject>> pooledLasers;    

    // Update is called once per frame
    void Start()
    {
        pooledLasers = new Dictionary<string, Queue<GameObject>>(); //LaserPool new Dictionery

        foreach (InspectPool lasPool in poolOfLasers)
        {
            Queue<GameObject> LaserPool = new Queue<GameObject>(); //Assign Pool obj with Prefab
            //Populate Pool
            for (int i = 0; i < lasPool.poolSize; i++)
            {
                GameObject LaserClone = Instantiate(lasPool.prefab);
                LaserClone.SetActive(false);
                LaserPool.Enqueue(LaserClone);
            }
            pooledLasers.Add(lasPool.nameTag, LaserPool); //Asign obj to Dictionery
        }
    }

    //Spawn and set Dictionery Objects
    public GameObject SpawnLaser(string nameTag, Vector3 position, Quaternion rotation)
    {
        //Check Key Assigment
        if (!pooledLasers.ContainsKey(nameTag))
        {
            Debug.LogWarning("Pool with tag " + nameTag + " doesn't exist");
            return null;
        }

        //Take Object from pool
        GameObject LaserSpawn = pooledLasers[nameTag].Dequeue();
        //Set Object properties
        LaserSpawn.transform.position = position;
        LaserSpawn.transform.rotation = rotation;
        LaserSpawn.SetActive(true);
        IPoolLaser pooledLaser = LaserSpawn.GetComponent<IPoolLaser>();

        if (pooledLaser != null)
        {
            pooledLaser.LaserPool();
        }

        //Put back to pool
        pooledLasers[nameTag].Enqueue(LaserSpawn);
        //Spawn Object
        return LaserSpawn;
    }

}
