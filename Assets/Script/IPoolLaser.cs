﻿using UnityEngine;

/// <summary>
/// LaserShots Pool Interface
/// </summary>
public interface IPoolLaser
{
    void LaserPool();
}
