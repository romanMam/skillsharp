﻿using UnityEngine;

/// <summary>
/// Pool Emiter Objects Interface
/// </summary>
public interface IPoolObj
{
    void ObjectPool();
}
