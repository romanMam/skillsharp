﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Game Boundry Script
/// </summary>
public class Boundary : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        other.gameObject.SetActive(false);
        Debug.Log(other.gameObject + " Leave playfield");
    }
}
