﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

/// <summary>
/// Menu and InGame interface script
/// </summary>
public class Interface : MonoBehaviour
{
    //Canvas and Menu Elements
    public GameObject GameUI;
    public GameObject PauseUI;
    public GameObject EndUI;
    public GameObject CreditUI;
    public GameObject MenuUI;

    //Scenes of the Game
    Scene Menu;
    Scene Game;

    //Time Refference
    public Text TimerTxt;
    public float TimeSeconds = 60.0f;

    public static int score; //Count Score
    public GameObject scoreTxt; //InGame Score Placement
    public GameObject finalScore; //Final Score Placement

    //Timer counter, convertion and ShowTime Courutine
    public IEnumerator StartCount(float TimeSeconds)
    {
        while (TimeSeconds > 0)
        {
            //Debug.Log("Countdown: " + TimeSeconds); //Log

            //Wait and count down
            yield return new WaitForSeconds(1.0f);
            TimeSeconds--;

            //Convert minutes and seconds
            int min = Mathf.FloorToInt(TimeSeconds / 60); //Divide the guiTime by sixty to get the minutes.
            int sec = Mathf.FloorToInt(TimeSeconds % 60);//Use the euclidean division for the seconds.

            //Convert float time to string
            TimerTxt.text = string.Format("Time: " + "\n" + "{0:00}:{1:00}", min, sec); //Show Converted Time

            scoreTxt.GetComponent<TextMeshPro>().text = "Score: " + "\n" + score; //Update Score
        }

        EndGame(); //Game End Function
    }

    void Start()
    {
    }

    //Main Menu Button
    public void PlayGame()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
    }

    #region InGame Pause UI
    //Pause Game Button OnScreen
    public void PauseGame()
    {
        if (PauseUI != null)
        {
            //Pause Time
            if (Time.timeScale == 1.0f)
                Time.timeScale = 0.0f;
            else
                Time.timeScale = 1.0f;

            bool isActive = PauseUI.activeSelf;
            PauseUI.SetActive(!isActive);
        }
    }

    public void BackToGame()
    {
        Time.timeScale = 1.0f;
        PauseUI.SetActive(false);
    }
    #endregion

    //Back to MainMenu 
    public void BackToMenu()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex - 1);
    }

    //To Menu From Credit
    public void ToMenuCan()
    {
        if (MenuUI != null)
        {
            Debug.Log("MenuCanvas if method");
            CreditUI.SetActive(false);
            MenuUI.SetActive(true);
        }
    }

    //Credit menu button
    public void CreditsBtn()
    {
        if (CreditUI != null)
        {
            Debug.Log("OpenCredit Panel if method");
            bool isActive = CreditUI.activeSelf;
            MenuUI.SetActive(false);
            CreditUI.SetActive(true);
        }
    }

    //End Game Method
    public void EndGame()
    {
        //Stop Time
        Time.timeScale = 0.0f;
        
        //Remove other Canvas
        GameUI.SetActive(false);
        EndUI.SetActive(true);

        //Set Score from game
        finalScore = GameObject.FindGameObjectWithTag("FinalScore");
        finalScore.GetComponent<Text>().text = "Score: " + score;

    }

    //Reload Game
    public void PlayAgain()
    {
        //Start Time
        Time.timeScale = 1.0f;
        //Reset Score 
        score = 0;
        //Reset Player
        PlayerScript.shipHealth = 100;
        GameObject.FindGameObjectWithTag("Player").SetActive(true);
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }

    //Update check pauseBtn, Check For Player Life.
    void Update()
    {
        //Pause Game - Escape Key
        if (Input.GetKeyDown("escape"))
        {
            PauseGame();
        }

        //End on Player Death
        if (GameObject.FindGameObjectWithTag("Player") == null)
        {
            EndGame();
        }
    }
}
