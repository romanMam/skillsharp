﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Background parallax scroll
/// </summary>
public class Background : MonoBehaviour
{
    public float flowSpeed;

    private Vector3 startPosition;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float newZPosition = Mathf.Repeat(Time.time * flowSpeed, 500);

        this.transform.position = startPosition + Vector3.back * newZPosition;
    }
}
