﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// FireShot Prefab Script - RigidBody, ShotSpeed, PoolObj Instance
/// </summary>
public class FireScript : MonoBehaviour, IPoolLaser
{
    public float shotSpeed;
    // Start is called before the first frame update
    public void LaserPool()
    {
        Rigidbody LaserShot = GetComponent<Rigidbody>();//Asteroid Rigidbody component
        LaserShot.velocity = Vector3.forward * shotSpeed;
    }

}
