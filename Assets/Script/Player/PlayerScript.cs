﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player Movements, Health and Sheild Paraments, ShotsTiming
/// </summary>
public class PlayerScript : MonoBehaviour
{
    public float shipSpeed; //Ship Speed (public set)
    public float shipTilt; //Ship Tilt (public set)
    public static int shipHealth = 100; //PlayerShip Health
    public static bool shipDeath;

    // ShipTilt Limits
    public float xTiltMin, xTiltMax, zTiltMin, zTiltMax;

    public Transform GunPosition; //Ship Guns position
    public GameObject PlayerExplosion; //Player Explosion

    public float ShotDelay; //User Shot Delay time
    private float nextShot; // Next shot time

    //Ship Movemant method class
    void ShipMovemant()
    {
        //Declear X,Z Axis from screen
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        //Ingame RigidBody component
        Rigidbody Ship = GetComponent<Rigidbody>();

        //Ship Velocity
        Ship.velocity = new Vector3(moveHorizontal, 0, moveVertical) * shipSpeed;
        //Ship Tilt
        Ship.rotation = Quaternion.Euler(moveVertical * shipTilt, 0, -moveHorizontal * shipTilt);

        //Ship Tilt limits X,Z Axis
        float xPosition = Mathf.Clamp(Ship.position.x, xTiltMin, xTiltMax);
        float zPosition = Mathf.Clamp(Ship.position.z, zTiltMin, zTiltMax);
        Ship.position = new Vector3(xPosition, Ship.position.y, zPosition);
    }

    //Shot method class
    void ShotCall()
    {
        //Check Fire Timing
        if (Input.GetButton("Fire1") && Time.time > nextShot)
        {
            //Pool ShotPrefab
            LaserPooler.LaserInstance.SpawnLaser("Lasers", GunPosition.position, Quaternion.identity);
            nextShot = Time.time + ShotDelay;
        }
    }
    
    //Check Player Health
    void CheckShip()
    {
        if (shipHealth <= 0)
        {
            shipDeath = true;
            Instantiate(PlayerExplosion, this.transform.position, Quaternion.identity);//Play DestroyPlayer Effect
            this.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        ShipMovemant();//PlayerMove method
        ShotCall();//FireShot method
        CheckShip();//ShipHealth Status
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Asteroid")
        {
            CheckShip(); 
        }
        if (other.gameObject.tag == "Asteroid" || shipHealth == 0)
        {
            return;//End Collision check
        }
    }
}
