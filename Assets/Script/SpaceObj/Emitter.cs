﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// SpaceObjects Emitter - Spawn Objects from Pools
/// </summary>
public class Emitter : MonoBehaviour
{
    public float minDelay, maxDelay;

    private float nextObject;

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Time.time > nextObject)
        {
            //Launch next Asteroid
            nextObject = Time.time + Random.Range(minDelay, maxDelay);

            float yPosition = transform.position.y;
            float zPosition = transform.position.z;
            float xPosition = Random.Range(-transform.localScale.x/2, transform.localScale.x/2);
            //Spawn Asteroid Obj from Pool Dictionary
            ObjDictionary.DictionaryInstance.ObjReuse("Asteroids", new Vector3(xPosition, yPosition, zPosition), Quaternion.identity);
        }
    }
}
