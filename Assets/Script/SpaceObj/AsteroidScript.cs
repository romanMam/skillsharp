﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Asteroid Prefab Script - RigidBody,Transfor Param, ObjPool, UpdateScore
/// </summary>
public class AsteroidScript : MonoBehaviour, IPoolObj
{
    public float rotationSpeed; //Asteroid rotation speed
    public float moveMinSpeed; //Asteroid move speed MIN
    public float moveMaxSpeed; //Asteroid move speed MAX

    public GameObject AsteroidExplosion; //Asteroid Explosion FX

    // Start is called before the first frame update
    public void ObjectPool()
    {
        Rigidbody Asteroid = GetComponent<Rigidbody>();//Asteroid Rigidbody component
        Asteroid.angularVelocity = Random.insideUnitSphere * rotationSpeed; //Asteroid velocity and movemant function
        Asteroid.velocity = Vector3.back * Random.Range(moveMinSpeed, moveMaxSpeed); //Asteroid velocity and movemant function
    }

    //Register Collider Triggers
    private void OnTriggerEnter(Collider other)
    {
        //Other asteroids and GameBoundary
        if (other.tag == "Asteroid" || other.tag == "Boundary")
        {
            return;//Do noting with other Asteroid. Exit method
        }
        //Trigger with Player
        if (other.tag == "Player")
        {
            PlayerScript.shipHealth -= 20; // Lower shipHealth on Collision
            Instantiate(AsteroidExplosion, transform.position, Quaternion.identity);//DestroiAsteroid Effect
            gameObject.SetActive(false); // Destroy Asteroid
        }
        //Trigger with LaserShot
        if (other.tag == "LaserPfb")
        {
            Instantiate(AsteroidExplosion, transform.position, Quaternion.identity);//DestroiAsteroid Effect
            other.gameObject.SetActive(false); // Destroy Asteroid
            gameObject.SetActive(false); // Destroy Shot
            
            //Add score for hit
            GameMain.score += Random.Range(1, 4);
        }
    }

}
