﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine;

/// <summary>
/// Main Game Script 
/// Contains : /// -Scene loading and storing; /// -UI Elements, controll; /// -GameTime; /// -GameScore; /// -Data storage and Logging;
/// ---
/// Create Data - 04/10/19
/// Author - Mamrukov R.
/// Update Data -
/// Update Info -
/// </summary>
public class GameMain : MonoBehaviour
{
    #region Deffine elements
    public Camera mainCamera;
    public GameObject Emitter;
    Animator cameraAnim;

    //UI Menues Prefabs
    #region UIPrefabs
    public GameObject GameUI;
    public GameObject PauseUI;
    public GameObject EndUI;
    public GameObject CreditUI;
    public GameObject MenuUI;
    #endregion

    //Time and Score data set
    public static int score;
    public float TimeSeconds = 60.0f;
    float StartTime;

    //Time and Score placement refference
    public TMP_Text TimePlace;
    public TMP_Text ScorePlace;
    public TMP_Text FinalScorePlace;
    #endregion

    //TimeCounter, convert and place data Courutine
    public IEnumerator TimeCount(float TimeSeconds)
    {
        while (TimeSeconds > 0.0f)
        {
            Debug.Log("Countdown: " + TimeSeconds); //Log

            //Convert minutes and seconds
            int min = Mathf.FloorToInt(TimeSeconds / 60); //Divide the guiTime by sixty to get the minutes.
            int sec = Mathf.FloorToInt(TimeSeconds % 60);//Use the euclidean division for the seconds.

            //Wait and count down
            yield return new WaitForSeconds(1.0f);
            
            TimeSeconds--;

            //Set Time to Time place
            TimePlace.text = string.Format("{0:00}:{1:00}", min, sec);
            Debug.Log("Time is " + min + " " + sec);
        }
        EndGame();
    }

    //Start Game Point
    void Start()
    {
        cameraAnim = mainCamera.GetComponent<Animator>();
        Time.timeScale = 1.0f;        
        StartTime = TimeSeconds;
        Emitter.SetActive(false);
    }

    //PlayGame - Menu Button
    public void PlayGame()
    {
        mainCamera.orthographic = true;
        mainCamera.orthographicSize = 45f;

        cameraAnim.Play("CameraMove");

        StartCoroutine(TimeCount(TimeSeconds));
        
        MenuUI.SetActive(false);
        GameUI.SetActive(true);

        SceneManager.LoadScene("SpaceScene", LoadSceneMode.Additive);
        Emitter.SetActive(true);
    }

    #region Pause Game
    //Pause Game Button OnScreen
    public void PauseGame()
    {
        if (PauseUI != null)
        {
            Time.timeScale = 1f;
            PauseUI.SetActive(false);
            GameUI.SetActive(true);
        }
        Time.timeScale = 0f;
        GameUI.SetActive(false);
        PauseUI.SetActive(true);
    }

    public void BackToGame()
    {
        Time.timeScale = 1f;
        PauseUI.SetActive(false);
        GameUI.SetActive(true);
    }
    #endregion

    //Back to GameMenu from Pause/EndGame 
    public void ToGameMenu()
    {
        if(EndUI != null)
        {
            EndUI.SetActive(false);
            score = 0;
            PlayerScript.shipHealth = 100;
            TimeSeconds = StartTime;
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    #region Credit Canvas
    //Open Credit
    public void CreditsBtn()
    {
        MenuUI.SetActive(false);
        CreditUI.SetActive(true);
    }

    //To MainMenu From CreditMenu
    public void ToMenuCan()
    {
        CreditUI.SetActive(false);
        MenuUI.SetActive(true);
    }
    #endregion

    //End Game Method
    public void EndGame()
    {
        //Stop Time
        Time.timeScale = 0.0f;
        //Change MainUI to EndUI Canvas
        GameUI.SetActive(false);
        EndUI.SetActive(true);
        //Set Score from game
        FinalScorePlace.text = "" + score;
    }

    #region PlayAgain
    // Rest Parameters Before PlayAgain
    public void ResetStats()
    {
        //Reset Time
        TimeSeconds = StartTime;
        //Reset PlayerHealth
        PlayerScript.shipHealth = 100;
        //Start Time
        Time.timeScale = 1.0f;
        //Reset Score 
        score = 0;
        PlayGame();
    }

    //Reload Game
    public void PlayAgain()
    {
        //Debug and UI Change
        EndUI.SetActive(false);
        Debug.Log("EndUI");
        ResetStats();
    }
    #endregion

    //Show ScoreUI
    public void ScoreShow()
    {
        ScorePlace.SetText("{0}", score);
    }

    //Update check pauseBtn, Check For Player Life.
    void Update()
    {
        //ScoreUI Update
        if (GameUI != null)
        {
            ScoreShow();
        }

        //Pause Game - Escape Key
        if (GameUI != null && Input.GetKeyDown("escape"))
        {
            PauseGame();
        }
        
        //End Time Manualy
        if (Input.GetKeyDown("o"))
        {
            TimeSeconds = 0.0f;
            Debug.Log("End Time and Game!");
        }

        //End on TimeIsOut
        if (TimeSeconds == 0.0f || PlayerScript.shipDeath == true)
        {
            EndGame();
        }
    }
}
